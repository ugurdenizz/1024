extends Node2D

# Hours Pointer
export (NodePath) var pivot_hours_path
onready var pivot_hours = get_node(pivot_hours_path)

# Minutes Pointer
export (NodePath) var pivot_minutes_path
onready var pivot_minutes = get_node(pivot_minutes_path)

# Seconds Pointer
export (NodePath) var pivot_seconds_path
onready var pivot_seconds = get_node(pivot_seconds_path)

# Pointer angles
var radian_seconds = null
var radian_minutes = null
var radian_hours = null

# Crazy time - Used to speed the clock up
var crazy_time = 0


# Start
func _ready():
	set_process(true)


# Processing
func _process(delta):
	# Uncomment line below to speed things up!
	# crazy_time -= delta*30
	
	# Get radians
	radian_seconds = -OS.get_time()["second"] * (PI/30) + crazy_time
	radian_minutes = -OS.get_time()["minute"] * (PI/30) + radian_seconds/60
	radian_hours = -OS.get_time()["hour"] * (PI/6) + radian_minutes/12
	
	# Assignment
	pivot_seconds.set_rot(radian_seconds)
	pivot_minutes.set_rot(radian_minutes)
	pivot_hours.set_rot(radian_hours)