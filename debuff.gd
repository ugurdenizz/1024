extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area_body_entered(body):
		if(body.is_in_group("player")):
			body.walk_speed=1
			body.run_speed=2



func _on_Area_body_exited(body):
		if(body.is_in_group("player")):
			body.walk_speed=3
			body.run_speed=7
		
