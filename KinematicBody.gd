extends KinematicBody
var direction = Vector3.BACK
var puppet_direction =Vector3.BACK
var velocity =  Vector3.ZERO
var puppet_velocity = Vector3.ZERO
var strafe_dir =  Vector3.ZERO
var strafe =  Vector3.ZERO
var position = Vector3.ZERO
var puppet_position = Vector3.ZERO

var aim_turn = 0
var vertical_velocity = 1
var gravity = 20
var movement_speed = 3
var walk_speed = 1.5
var run_speed = 4
var acceleration = 4
var angular_acceleration =6

enum {
	IDLE,
	WANDER
}
var state = WANDER

const ACCELERATION = 3
const MAX_SPEED = 5
const TOLERANCE = 4.0

var start_position = Vector3.ZERO
var target_position = Vector3.ZERO

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
#	rset_config("puppet_velocity", MultiplayerAPI.RPC_MODE_REMOTESYNC)
#	rset_config("puppet_direction", MultiplayerAPI.RPC_MODE_REMOTESYNC)
#	rset_config("puppet_position", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	pass
func _physics_process(delta):

	$viking_C_position.rotation.y = lerp_angle($viking_C_position.rotation.y, atan2(target_position.x, target_position.z) - rotation.y, delta * angular_acceleration)
	match state:
		IDLE:
			state = WANDER
			# Maybe wait for X seconds with a timer before moving on
			update_target_position()

		WANDER:
			accelerate_to_point(target_position, ACCELERATION * delta)

			if is_at_target_position():
				state = IDLE

	position=puppet_position
	velocity=puppet_velocity
	set_translation(puppet_position)	

#func _process(delta):
#	pass
func update_target_position():
	var target_vector = Vector3(rand_range(-50, 50), 0,rand_range(-50, 50))
	target_position = start_position + target_vector
	target_position=puppet_position

func is_at_target_position(): 
	# Stop moving when at target +/- tolerance
	return (target_position - get_translation()).length() < TOLERANCE
	
func accelerate_to_point(point, acceleration_scalar):
	var direction = (point - get_translation()).normalized()
	var acceleration_vector = direction * acceleration_scalar
	accelerate(acceleration_vector)
remotesync func cleared():
	
	queue_free()
func accelerate(acceleration_vector):
	velocity = acceleration_vector
	velocity=puppet_velocity
	var collision=move_and_collide(velocity)
	if collision is KinematicCollision and collision.collider is Node:
		if(collision.collider.is_in_group("player")):
			print("hello")
			
		
	

	
#	velocity = velocity.clamped(MAX_SPEED)
