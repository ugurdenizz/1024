extends Spatial

var bodyIn


func _ready():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var yalaka=rng.randi_range(0,1)
	if(yalaka==1):
		var yalak=preload("res://yalaka.tscn").instance()
		self.add_child(yalak)
	
func _on_Area_body_entered(body):
	if(body.is_in_group("player")):
		var nodeOffice=get_node("/root/Office")
		var workload=nodeOffice.workload[self.name]
		if(workload==1):
			var playerNotification=get_node("/root/Office/Slave/camroot/workload")
			playerNotification.visible =true
		bodyIn=1
	
func _on_Area_body_exited(body):
	if(body.is_in_group("player")):
		var playerNotification=get_node("/root/Office/Slave/camroot/workload")
		playerNotification.visible =false
		bodyIn=0
func _physics_process(delta):
	if bodyIn==1:
		 if Input.is_action_pressed("use"):
			 var Mat=self.get_node("laptop/Box001/Box002").get_surface_material(0)
			 Mat.set_shader_param("col",Vector3(0.0,0.0,1.0))
			 var clock_minutes=get_node("/root/Office/clock/pivot_minutes")
			 clock_minutes.rotation_degrees+=2.0
