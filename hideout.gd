extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var Mat=self.get_node("roof").material
	Mat.set_shader_param("alpha",1)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area_body_entered(body):
	if(body.is_in_group("player")):
		var Mat=self.get_node("roof").material
		Mat.set_shader_param("alpha",.2)


func _on_Area_body_exited(body):
	if(body.is_in_group("player")):
		var Mat=self.get_node("roof").material
		Mat.set_shader_param("alpha",1)
